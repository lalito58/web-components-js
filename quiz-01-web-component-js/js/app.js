console.log("app js");

class View {

  static createElement(type, id, classes, content) {
    let newElement = document.createElement(type);
    if(id != '') {  
      newElement.id = id;
    }
    if(classes.length > 0) {
      newElement.classList.add(...classes)
    }
    newElement.textContent = content;
    return newElement;
  }

  static addElementInView(mainElement, newElement) {
    mainElement.appendChild(newElement);
    return mainElement;
  }

  static returnStyle() {
    return `
    .container {
      border:1px solid lightgray;
      padding: 5px;
    }
    .message {
      /*border:1px solid blue;*/
      margin: 10px;
      padding: 5px 0px;
    }
    .instruction {
      border-top:1px solid green;
      margin: 10px;
      padding: 10px 0px;
    }
    .hide{
      display: none;
    }
    .red {
      color:red;
    }`;
  }

}

class NuevoComponente extends HTMLElement {

  //especificando un método estático get observedAttributes dentro de
  //la clase del elemento personalizado, esto devolvería una matriz que
  //contenga los nombres de los atributos que desea observar... :3
  static get observedAttributes() {
    return ['name'];
  }

  //Constructor de la clase del componente, con instancia de HTMLElement
  constructor() {
    super();
    //this.createViewInBody();
    //console.log("Constructor "+this.getAttribute('name'));
  }


  createBtnSubmit(id, classes, content, mainDiv) {
    let btn = View.createElement('button', id, classes, content);
    btn.addEventListener('click', () => {
      //alert("Clic "+this.setAttribute('name', "NAyeli"));
      if(this.mainInput.value != '') {
        /*if(this.getAttribute('name') != null) {
          this.createMessage(mainDiv);  
        }*/
        //alert(this.mainInput.value);
        this.setAttribute('name', this.mainInput.value);
        this.span.innerHTML = this.mainInput.value;
      }else {
        console.log("Agregue un nombre!");
      }
    });
    return btn; 
  }

  // INIT: ciclo de vida del componente
  //Componente agregado al dom...loaded
  connectedCallback() {
    console.log("Add new component");
    //Creamos el shadow para encapsular nuestro componente
    const shadow = this.attachShadow({mode: 'open'});
    
    let mainDiv = View.createElement('div', 'container', ['container'], '');
    //console.log(this.getAttribute('name'));

    let messageDiv = View.createElement('div', 'message', ['message'], 'Mi nombre es ');
    let span = View.createElement('span', 'textSpan', ['textSpan'], '');
    span.innerHTML = this.getAttribute('name');
    this.span = span;
    messageDiv = View.addElementInView(messageDiv, span);
    /*if(this.getAttribute('name') == null) {
      console.log("Hola");
      messageDiv.classList.toggle('hide');
    }*/
    mainDiv = View.addElementInView(mainDiv, messageDiv);

    let styleElement = View.createElement('style', '', [], View.returnStyle());
    mainDiv = View.addElementInView(mainDiv, styleElement);

    let instructionDiv = View.createElement('div', 'instruction', ['instruction'], 'Introduce un nombre abajo');
    mainDiv = View.addElementInView(mainDiv, instructionDiv);
    
    let input = View.createElement('input', 'name', ['name'], '');
    input.type = 'text';
    input.placeholder = 'Carnal ingresa un nombre';
    input.setAttribute('value', '');
    this.mainInput = input;
    mainDiv = View.addElementInView(mainDiv, input);
    //le pondre name al input?
    
    let classes = ['changeName', 'ads'];
    let btn = this.createBtnSubmit('changeName', classes, 'Cambiar nombre', mainDiv);
    mainDiv = View.addElementInView(mainDiv, btn);

    shadow.appendChild(mainDiv);
  }

  //Cuando el componente es eliminado del dom
  //disconnectedCallback() {}

  //se ejecuta cada vez que uno de los atributos del elemento sufre algún cambio
  //y para que se active cada que un atributo cambie es necesario observar los
  //atributos, y esto se hace...
  attributeChangedCallback(name, oldValue, newValue) {
    console.log(`${name} was changed from ${oldValue} to ${newValue}!`);
    if(newValue != null) {
      if(newValue.toUpperCase() === 'Manuel'.toUpperCase()) {
        this.span.className = 'red';
      }else{
        this.span.classList.remove('red');
      }  
    }
  }
  // END: ciclo de vida del componente
  

}

customElements.define('nuevo-componente', NuevoComponente);