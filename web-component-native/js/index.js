//console.log("Hola Mundo!");
//declaramos nuesto elementoClass - ejemplo de la diapositiva de enrique
class NuevoElementoWeb extends HTMLElement {//First Step

  //declaramos nuestro constructor
  constructor() {//Second Step
    super();//instanciamos la clase,interfaz HTMLElement - second step
    const shadow = this.attachShadow( {mode: 'open'} );//Second step
    const div = document.createElement('div');//second step
    div.textContent = 'Web Component With JS Native :3';//second step
    shadow.appendChild(div);//second step
  }//Second Step

  connectedCallback() {//third step
    console.log("Load NuevoElementoWeb agregado");
  }

  disconnectedCallback() {//third step
    console.log("remove NuevoElementoWeb");
  }

  attributeChangedCallback(name, oldValue, newValue) {//third step
    console.log("Cambian propiedades", name, oldValue, newValue);
  }

}//First Step

customElements.define('nuevo-elemento-web', NuevoElementoWeb);//First Step

//para ejecutar el disconnectedCallback podemos hacer esto en la consola:
//document.body.removeChild(document.querySelector('nombre del componente web'));